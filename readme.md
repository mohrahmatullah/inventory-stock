## DOKUMENTASI INSTALL

## Membangun Aplikasi Inventory Stok Menggunakan : 
	- Framework Laravel 5.7
	- PHP php:7.3-apache
	- MYSQL 5.7.34
	- Dashboard AdminLTE 2

## Project inventory stock
Please make sure it is connected to the internet

### Project install
		
		Extract file 
		

### If Docker server apache
Masukan perintah berikut pada file docker untuk menginstal ekstensi yang dibutuhkan

		RUN docker-php-ext-install pdo pdo_mysql mysqli gettext session bcmath calendar exif pcntl shmop sockets sysvmsg sysvsem sysvshm 

		RUN apt-get update && \
		    apt-get install -y libxml2-dev && \
		    docker-php-ext-install soap
		RUN docker-php-ext-install intl

		# packages
		RUN apt-get update \
		  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
		  # needed for gd
		  libfreetype6-dev \
		  libjpeg62-turbo-dev \
		  libpng-dev \
		  && rm -rf /var/lib/apt/lists/*

		# GD
		RUN docker-php-ext-configure gd --with-freetype-dir=/usr --with-jpeg-dir=/usr --with-png-dir=/usr \
		  && docker-php-ext-install -j "$(nproc)" gd

		# Install XSL
		RUN apt-get update && \
		    apt-get install -y libxslt1-dev && \
		    docker-php-ext-install xsl && \
		    apt-get remove -y libxslt1-dev icu-devtools libicu-dev libxml2-dev && \
		    rm -rf /var/lib/apt/lists/*

		RUN apt-get update && \
			apt-get install -y \
		        libzip-dev \
		        zip \
		  && docker-php-ext-install zip

		RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

### Update composer
		
		composer update
		
### Database connection
Create database
		
		Example name database : apps-inventory

Copy file .env.example to .env and enter your information

		
		DB_DATABASE=apps-inventory
		DB_USERNAME=root
		DB_PASSWORD=123
		

### Cache clear
		
		php artisan config:cache
		
### If Use Docker
		
		docker exec -it container_id bash

### Make migrate
		
		php artisan migrate
		
### APP_KEY

		php artisan key:generate

### Make seed
		
		php artisan db:seed
		

### Or export database on directory
		
		sql/apps-inventory.sql
		

### If use linux

		php artisan route:clear
		php artisan config:clear
		php artisan cache:clear
		chmod -R 777 storage
		chmod -R 777 bootstrap/cache

### Run project
		
		php artisan serve
		  
### Login info for admin
		
		email    : admin@email.com
		password : 123456
		
### Login info for user
		
		email    : user@email.com
		password : 123456
		

## Penjelasan
Didalam aplikasi ini terdapat fitur-fitur berikut ini :

- Login
- 2 Role Admin dan Staff

- Dashboard
- Modul Category
1. CRUD menggunakan AJAX JQUERY dengan menggunakan modal bootstrap
2. Export Ke PDF 
3. Export Ke Excel

- Modul Product
1. CRUD menggunakan AJAX JQUERY dengan menggunakan modal bootstrap
2. Qty product kosong karena harus ada transaksi product in

- Modul Customer
1. CRUD menggunakan AJAX JQUERY dengan menggunakan modal bootstrap
2. Export PDF 
3. Export Ke Excel
4. Import Ke Data Dari Excel Ke Sistem

- Modul User
1. CRUD menggunakan AJAX JQUERY dengan menggunakan modal bootstrap
2. Modul user hanya ada di akses admin saja

- Modul Supplier
1. CRUD menggunakan AJAX JQUERY dengan menggunakan modal bootstrap
2. Export PDF 
3. Export Ke Excel
4. Import Dari Excel Ke Sistem

- Modul Product Out
1. CRUD menggunakan AJAX JQUERY dengan menggunakan modal bootstrap
2. Export PDF 
3. Export Excel
4. Export Invoice Product Out
		
		Export invoice hanya status yang sudah di approve oleh admin

5. List product hanya qty product lebih dari nol
6. Akses approve hanya ada di admin
7. Ketika sudah di approve oleh admin Qty product akan berkurang

- Modul Product In
1. CRUD menggunakan AJAX JQUERY dengan menggunakan modal bootstrap
2. Export PDF 
3. Export Excel
4. Export Invoice Product In 

		Export invoice hanya status yang sudah di approve oleh admin
		
5. Akses approve hanya ada di admin
6. Ketika sudah di approve oleh admin Qty product akan bertambah 

Package yang digunakan 
- https://github.com/barryvdh/laravel-dompdf
- https://github.com/Maatwebsite/Laravel-Excel
- https://github.com/yajra/laravel-datatables
- https://github.com/LaravelCollective/html