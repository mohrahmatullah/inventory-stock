<?php

namespace App\Http\Controllers;


use App\Exports\ExportProdukMasuk;
use App\Product;
use App\Product_Masuk;
use App\Supplier;
use PDF;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Auth;


class ProductMasukController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin,staff');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('nama','ASC')
            ->get()
            ->pluck('nama','id');

        $suppliers = Supplier::orderBy('nama','ASC')
            ->get()
            ->pluck('nama','id');

        $invoice_data = Product_Masuk::where('status', '1')->get();
        return view('product_masuk.index', compact('products','suppliers','invoice_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id'     => 'required',
            'supplier_id'    => 'required',
            'qty'            => 'required',
            'tanggal'        => 'required'
        ]);

        Product_Masuk::create($request->all());

        // $product = Product::findOrFail($request->product_id);
        // $product->qty += $request->qty;
        // $product->save();

        return response()->json([
            'success'    => true,
            'message'    => 'Products In Created'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_masuk = Product_Masuk::find($id);
        return $product_masuk;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_id'     => 'required',
            'supplier_id'    => 'required',
            'qty'            => 'required',
            'tanggal'        => 'required'
        ]);

        $pm = Product_Masuk::findOrFail($id);
        if($pm->status == 1){
            $product = Product::findOrFail($request->product_id)->first();
            $product->qty = $product->qty - $pm->qty;
            $product->save(); 
        }          
        if($pm){
            $product_masuk = Product_Masuk::findOrFail($id);
            $product_masuk->status = '0';
            $product_masuk->update($request->all());
            return response()->json([
                'success'    => true,
                'message'    => 'Product In Updated'
            ]);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_masuk = Product_Masuk::findOrFail($id);
        if($product_masuk->status == 1){
            $product = Product::findOrFail($product_masuk->product_id)->first();
            $product->qty = $product->qty - $product_masuk->qty;
            $product->save();

            if($product){
                Product_Masuk::destroy($id);

                return response()->json([
                    'success'    => true,
                    'message'    => 'Products In Deleted'
                ]);
            } 
        }else{
            Product_Masuk::destroy($id);
            return response()->json([
                'success'    => true,
                'message'    => 'Products In Deleted'
            ]);
        }
              
    }



    public function apiProductsIn(){
        $product = Product_Masuk::all();

        return Datatables::of($product)
            ->addColumn('products_name', function ($product){
                return $product->product->nama;
            })
            ->addColumn('supplier_name', function ($product){
                return $product->supplier->nama;
            })
            ->addColumn('status', function ($product){

                if($product->status == 0){
                    $status = '<a class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i> Belum di Approve </a>';
                }else{
                    $status = '<a class="btn btn-success btn-xs"><i class="glyphicon glyphicon-ok"></i> Sudah di Approve </a>';
                }
                
                return $status;
            })
            ->addColumn('action', function($product){
                $html = '<a onclick="showForm('. $product->id .')" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i> Show</a> ';
                $html .= '<a onclick="editForm('. $product->id .')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a> ';
                $html .= '<a onclick="deleteData('. $product->id .')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
                if(Auth::user()->role == 'admin'){
                    $html .= '<a onclick="approveData('. $product->id .')" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-ok"></i> Approve</a>' ;
                }
                
                return $html;
                // return '<a onclick="showForm('. $product->id .')" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i> Show</a> ' .
                //     '<a onclick="editForm('. $product->id .')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                //     '<a onclick="deleteData('. $product->id .')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a> '  .
                //     '<a onclick="approveData('. $product->id .')" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-ok"></i> Approve</a> ';


            })
            ->rawColumns(['products_name','supplier_name','status','action'])->make(true);

    }

    public function exportProductMasukAll()
    {
        $product_masuk = Product_Masuk::where('status', '1')->get();
        $pdf = PDF::loadView('product_masuk.productMasukAllPDF',compact('product_masuk'));
        return $pdf->download('product_masuk.pdf');
    }

    public function exportProductMasuk($id)
    {
        $product_masuk = Product_Masuk::findOrFail($id);
        $pdf = PDF::loadView('product_masuk.productMasukPDF', compact('product_masuk'));
        return $pdf->download($product_masuk->id.'_product_masuk.pdf');
    }

    public function exportExcel()
    {
        return (new ExportProdukMasuk)->download('product_masuk.xlsx');
    }

    public function statusApprove($id)
    {
        $p = Product::findOrFail($product->product_id);
        $product = Product_Masuk::findOrFail($id);
        if($product->qty > 0){
            $product->status = '1';
            $product->save();

            if($product){
                $p->qty += $product->qty;
                $p->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Products Sudah Di Approve'
                ]);
            }   
        }else{
            return response()->json([
                'success'    => false,
                'message'    => 'Products Qty Di periksa kembali'
            ]);
        }
            
    }
}
